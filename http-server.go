package main

import (
    "fmt"
    "net/http"
		"time"
)

func health(w http.ResponseWriter, req *http.Request) {

    fmt.Fprintf(w, "server is currently healthy\n")
}

func date(w http.ResponseWriter, req *http.Request){
	currentTime := time.Now()
	fmt.Fprintf(w, "Current time is :", currentTime.Format("2006-01-02 15:04:05"),"\n")
	fmt.Println("hallo", currentTime.String())
}

func headers(w http.ResponseWriter, req *http.Request) {

    for name, headers := range req.Header {
        for _, h := range headers {
            fmt.Fprintf(w, "%v: %v\n", name, h)
        }
    }
}

func main() {

    http.HandleFunc("/health", health)
    http.HandleFunc("/headers", headers)
		http.HandleFunc("/date", date)

    http.ListenAndServe(":8090", nil)
}