package main

import (
	"encoding/json"
	"log"
	"net/http"
	"fmt"
	"time"
	"syscall"
	"github.com/zcalusic/sysinfo"
)

func main() {
	http.HandleFunc("/", handlerIndex)
	http.HandleFunc("/health", health)
	http.HandleFunc("/print", handlePrint)
	http.HandleFunc("/date", handleDate)
	http.HandleFunc("/shell", handleShell)

	fmt.Println("server started at localhost:9000")
	http.ListenAndServe(":9000", nil)
}

func handleShell(w http.ResponseWriter, r *http.Request){
	var si sysinfo.SysInfo

	si.GetSysInfo()

	data, err := json.MarshalIndent(&si, "", " ")
	if err != nil{
		log.Fatal(err)
	}

	fmt.Println(string(data))
}

func handlerIndex(w http.ResponseWriter, r *http.Request){
	var message = "welcome"
	w.Write([]byte(message))
}

func health(w http.ResponseWriter, req *http.Request) {

	fmt.Fprintf(w, "server is currently healthy\n")
}

func handlePrint(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
			decoder := json.NewDecoder(r.Body)
			payload := struct {
					Name   string `json:"name"`
					Age    int    `json:"age"`
					Gender string `json:"gender"`
			}{}
			if err := decoder.Decode(&payload); err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
			}

			jsonResp, err := json.Marshal(payload)
			if err != nil {
				log.Fatalf("Error happened in JSON marshal. Err: %s", err)
			}
			w.Write(jsonResp)
			return
	}

	http.Error(w, "Only accept POST request", http.StatusBadRequest)
}

func handleDate(w http.ResponseWriter, req *http.Request){
	currentTime := time.Now()
	fmt.Fprintf(w, "Current time is :", currentTime.Format("2006-01-02 15:04:05"),"\n")
	fmt.Println("hallo", currentTime.String())
}