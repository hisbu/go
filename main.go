package main

import "fmt"
import "net/http"
import "encoding/json"

func handlerIndex(w http.ResponseWriter, r *http.Request){
	var message = "welcome"
	w.Write([]byte(message))
}

func handlerHello(w http.ResponseWriter, r *http.Request){
	var message = "hello World"
	w.Write([]byte(message))
}

func handlerGet(w http.ResponseWriter, r *http.Request){
	data := [] struct {
		name string
		age int
	}{
		{"Richard", 24},
		{"Jason", 23},
		{"Drake", 22},
	}
	jsonInBytes, err := json.Marshal(data)
	// fmt.Printf(jsonInBytes)
	if err != nil{
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type","application/json")
	w.Write(jsonInBytes)
}

func main(){
	http.HandleFunc("/", handlerIndex)
	http.HandleFunc("/index", handlerIndex)
	http.HandleFunc("/hello", handlerHello)
	http.HandleFunc("/print", handlerGet)

	var address = "localhost:9000"
	fmt.Printf("server started at %s\n", address)
	err := http.ListenAndServe(address, nil)
	if err != nil{
		fmt.Println(err.Error())
	}
}